<?php

namespace App\Http\Controllers;

use App\Models\Upload;

class UploadController extends Controller
{
    public function index_view ()
    {
        return view('pages.upload.upload', [
            'upload' => Upload::class
        ]);
    }
}